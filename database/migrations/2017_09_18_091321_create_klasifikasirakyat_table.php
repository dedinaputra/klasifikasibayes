<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateKlasifikasiRakyatTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Model::unguard();
        Schema::create('klasifikasirakyat',function(Blueprint $table){
            $table->increments("id");
            $table->string("bangunan");
            $table->enum("bakar", ["Kayu Bakar", "Gas LPG", "Kompor Listrik", ]);
            $table->enum("lantai", ["Ubin", "Plester", "Tanah", ]);
            $table->string("daging");
            $table->enum("kategori", ["Kaya", "Sedang", "Miskin", ]);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('klasifikasirakyat');
    }

}