<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('tampil');
});

Route::get('/','KlasifikasiController@index');
Route::post('/analisa','KlasifikasiController@analisa');
Route::post('/simpan','KlasifikasiController@simpan');
Route::post('/simpanbaru','KlasifikasiController@simpanbaru');
Route::get('/tambah','KlasifikasiController@tambah');

