
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}" />

<style>
a{
	color:white;
}
a:hover{
	text-decoration:none;
	color:white;
}

</style>
</head>
<body style="background-color:#0046b7">
<div class="container">
<!-- Baris 1 Judul -->
	<div class="row" style="margin-top:5%;height:100%">
	<div class="col-md-12" style="height:100%;background-color: maroon;text-align:center;color:white">
	<h3 style="color:#fcc920">Sistem Klasifikasi Kemampuan Ekonomi Individu Menggunakan Metode Naive Bayes</h3>
	Kelompok 9, Teknik Informatika J | Dedin Anike Putra | Erdian Sahlan | Farhan Caesarian
	</div>
	</div>

	<!-- Baris 2 isian -->
	<div class="row">
		<div class="col-md-12" style="background-color: black;padding:10px;color:white;text-align:center;height:100%">
		<form method="POST" action ="{{URL('/analisa')}}">
		{{csrf_field()}}
			Luas Bangunan <input style="color:black" type="number" step="0.01" name="bangunan">

			Bahan Bakar yang Digunakan 
			<select name="bakar" style="color:black">
			<option name="bakar" value="Kayu Bakar">Kayu Bakar</option>
			<option name="bakar" value="Gas LPG">Gas LPG</option>
			<option name="bakar" value="Kompor Listrik">Kompor Listrik</option><
			</select>  
			

			Jenis Lantai 
			<select name="lantai" style="color:black">
			<option name="lantai" value="Ubin">Ubin</option>
			<option name="lantai" value="Tanah">Tanah</option>
			<option name="lantai" value="Plester">Plester</option>
			</select>
			


			Frekuensi Makan Daging <input type="number" step="0.01" name="daging" style="color:black"><br><br>
			<div class="col-md-5"></div>
			<input style="float:left" type="submit" value="Analisa" class="btn btn-primary">
		</form>
			<a href="{{URL('tambah')}}"><button style="float:left" class="btn btn-warning">Tambah Data</button></a>
		
	  </div>
	</div>

	<!-- Baris 3 kalau kosong -->
	@if (count($errors)>0)
	<div class="row">
	   	<div class="alert alert-danger">
			<ul>@foreach ($errors->all() as $error)
	  			<li>{{$error}}</li>
	  			@endforeach
			</ul>
	    </div>
	</div>
	@endif
	

	<!-- Baris 4 penambahan -->
	@if(isset($tambah))
	<div class="row">
	
	<div class="col-md-12" style="background-color: maroon;padding:10px;color:white;text-align:center">
	<h3 style="color:#fcc920">Tambah Data</h3>
		<table style="margin:auto;color:white;text-align:left">
				<form method="POST" action="{{URL('simpanbaru')}}">
					{{csrf_field()}}
					<tr><td>Luas Bangunan </td><td><input type="number" step="0.01" name="bangunan" style="color:black"></td></tr>

					<tr><td>Bahan Bakar yang Digunakan </td><td>
					<select name="bakar" style="color:black">
					<option name="bakar" value="Kayu Bakar">Kayu Bakar</option>
					<option name="bakar" value="Gas LPG">Gas LPG</option>
					<option name="bakar" value="Kompor Listrik">Kompor Listrik</option><
					</select>  
					</td></tr>

					<tr><td>Jenis Lantai</td><td> 
					<select name="lantai" style="color:black">
					<option name="lantai" value="Ubin">Ubin</option>
					<option name="lantai" value="Tanah">Tanah</option>
					<option name="lantai" value="Plester">Plester</option>
					</select></td></tr>
					


					<tr><td>Frekuensi Makan Daging </td><td><input type="number" step="0.01" name="daging" style="color:black"></td></tr>
					
					<tr><td>Kategori</td><td> 
					<select name="kategori" style="color:black">
					<option name="kategori" value="Miskin">Miskin</option>
					<option name="kategori" value="Sedang">Sedang</option>
					<option name="kategori" value="Kaya">Kaya</option>
					</select></td></tr>

					<br><br>
					<tr><td><input type="submit" value="Simpan" class="btn btn-primary">
			 	</form>
			 	<a href="{{URL('/')}}"><button class="btn btn-warning">Batal</button></a></td></tr>
		</table> 
	 
	 </div>
	 </div>
	@endif
	

	<!-- Baris 5 hasil analisis -->
	@if(isset($hasil))
	<div class="row">
	<div class="col-md-12" style="background-color: maroon;padding:10px;color:white;text-align:center">
		Hasil Klasifikasi :<br>
		<table style="text-align:left;margin:auto;color:white">
			<tr><th>Miskin</th><td>{{$miskin}}</td></tr>
			<tr><th>Sedang</th><td>{{$sedang}}</td></tr>
			<tr><th>Kaya</th><td>{{$kaya}}</td></tr>
			

		</table>
			Kategori
			<h2 style="color:#fcc920">{{$hasil}}</h2>

				<form method="POST" action="{{URL('simpan')}}">
				 	{{csrf_field()}}
					<input type="text" name="bangunan" value ="{{$bangunan}}" hidden>
					<input type="text" name="daging" value ="{{$daging}}" hidden>
					<input type="text" name="lantai" value ="{{$lantai}}" hidden>
					<input type="text" name="bakar" value ="{{$bakar}}" hidden>
					<input type="text" name="klas" value ="{{$hasil}}" hidden>
					<input type="submit" value="Simpan Klasifikasi" class="btn btn-primary"><a href="{{URL('/')}}"><button class="btn btn-warning">reset</button></a>
			 	</form>
	 </div>
	 </div>
	@endif

	<!-- Baris 6 data -->
	<div class="row">
	<div class="col-md-12" style="background-color: #f9f9f9;color:black">
		<div class="table-responsive">
			<table class="table">
				<thead>
					<tr>
						<th>Luas Bangunan</th>
						<th>Jenis Bahan Bakar</th>
						<th>Jenis Lantai</th>
						<th>Jumlah Konsumsi Daging/minggu</th>
						<th>Kategori</th>
					</tr>
				</thead>
				<tbody>
				@foreach($data as $row)
				<tr>
				<td>{{$row->bangunan}}</td>
				<td>{{$row->bakar}}</td>
				<td>{{$row->lantai}}</td>
				<td>{{$row->daging}}</td>
				<td>{{$row->kategori}}</td>
				</tr>
				@endforeach



				</tbody>
			</table>
		</div>
	</div>

	</div>

</div>
</body>
</html>