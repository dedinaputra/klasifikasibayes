<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

class KlasifikasiRakyat extends Model {

    use SoftDeletes;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    protected $table    = 'klasifikasirakyat';
    
    protected $fillable = [
          'bangunan',
          'bakar',
          'lantai',
          'daging',
          'kategori'
    ];
    
    public static $bakar = ["Kayu Bakar" => "Kayu Bakar", "Gas LPG" => "Gas LPG", "Kompor Listrik" => "Kompor Listrik", ];
    public static $lantai = ["Ubin" => "Ubin", "Plester" => "Plester", "Tanah" => "Tanah", ];
    public static $kategori = ["Kaya" => "Kaya", "Sedang" => "Sedang", "Miskin" => "Miskin", ];


    public static function boot()
    {
        parent::boot();

        KlasifikasiRakyat::observe(new UserActionsObserver);
    }
    
    
    
    
}