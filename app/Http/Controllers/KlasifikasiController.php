<?php

namespace App\Http\Controllers;
use App\KlasifikasiRakyat;
use Illuminate\Support\Facades\DB;


use Illuminate\Http\Request;

class KlasifikasiController extends Controller
{
    public function index(){
    	$data = DB::table('klasifikasirakyat')->orderBy('id','desc')->get();
        return view('tampil')->with('data',$data);

        
    }


    public function analisa(Request $request){
        $validate = \Validator::make($request->all(), [
                'bangunan' => 'required',
                'daging' => 'required',
                'lantai' => 'required',
                'bakar' => 'required',
            ],$messages = [
                'required' => "Data :attribute kosong",
            ]);
        if($validate->fails()){
            $errors=$validate->messages();
            return redirect()->back()->with('errors',$errors);
        }

        $sedang = $this->naiv($request->bangunan,$request->bakar,$request->lantai,$request->daging,'Sedang');
        $kaya = $this->naiv($request->bangunan,$request->bakar,$request->lantai,$request->daging,'Kaya');
        $miskin = $this->naiv($request->bangunan,$request->bakar,$request->lantai,$request->daging,'Miskin');
        $terbesar = max($sedang,$kaya,$miskin);
        $hasil="tidaksama";
        if($terbesar==$sedang){
            $hasil="Sedang";
        }else if($terbesar==$kaya){
            $hasil="Kaya";
        }else{
            $hasil="Miskin";
        }
        $data = DB::table('klasifikasirakyat')->orderBy('id','desc')->get();
        return view('tampil')->with('data',$data)->with('sedang',$sedang)->with('kaya',$kaya)->with('miskin',$miskin)->with('hasil',$hasil)->with('bangunan',$request->bangunan)->with('lantai',$request->lantai)->with('bakar',$request->bakar)->with('daging',$request->daging);

    }
    public function tambah(){
        $data = DB::table('klasifikasirakyat')->orderBy('id','desc')->get();
        return view('tampil')->with('tambah','ada')->with('data',$data);
    }
    public function simpan(Request $request){
        $data = [
            'bangunan' => $request->bangunan,
            'daging' => $request->daging,
            'lantai' => $request->lantai,
            'bakar' => $request->bakar,
            'kategori' => $request->klas,
        ];
        $store = \App\KlasifikasiRakyat::insert($data);
        return redirect()->to('/');

    }
    public function simpanbaru(Request $request){
         $validate = \Validator::make($request->all(), [
                'bangunan' => 'required',
                'daging' => 'required',
                'lantai' => 'required',
                'bakar' => 'required',
                'kategori' => 'required',
            ],$messages = [
                'required' => "Data :attribute kosong",
            ]);
        if($validate->fails()){
            $errors=$validate->messages();
            return redirect()->back()->with('errors',$errors);
        }
        $data = [
            'bangunan' => $request->bangunan,
            'daging' => $request->daging,
            'lantai' => $request->lantai,
            'bakar' => $request->bakar,
            'kategori' => $request->kategori,
        ];
        $store = \App\KlasifikasiRakyat::insert($data);
        return redirect()->to('/');

    }


    public function likelihood($varian,$data,$ratarata){
        $bawah = sqrt(2*pi()*$varian);
        $pangkat = (pow($data-$ratarata,2))/(2*$varian);
        $likelihood = 1/$bawah*pow(M_E,-$pangkat);
        return $likelihood;

    }

    public function naiv($bangunan, $bakar, $lantai, $daging,$klas){
    	//p(sedang)
    	$data= \App\KlasifikasiRakyat::all()->count(); //jumlah semua data
    	$jumlahklas = \App\KlasifikasiRakyat::where('kategori',$klas)->count();// jumlah kelas x
    	$prior = $jumlahklas/$data; //prior
        // return $prior;

        //p($bangunan|Sedang)
        //menghitung varian data bangun sedang
        $avgbangunan = DB::table('klasifikasirakyat')->where('kategori',$klas)->avg('bangunan');//rata rata tinggi bangunan
        $sum=0; //sigma
        $data = DB::table('klasifikasirakyat')->select('bangunan')->where('kategori',$klas)->get();
            foreach($data as $row){
                $sum+=pow($row->bangunan-$avgbangunan,2);
            }
        $varian = $sum/($jumlahklas-1);//varian
        $likelihood= $this->likelihood($varian,$bangunan,$avgbangunan); //likelihood bangunan x
        $pbangunan = $prior*$likelihood;
        // return $pbangunan;

    	//p($bakar|Sedang)
    	$bahanbakar = \App\KlasifikasiRakyat::where('bakar',$bakar)->where('kategori',$klas)->count();
    	$pbakar = $bahanbakar/$jumlahklas;
        // return $pbakar;

    	//p($lantai|Sedang)
    	$jenislantai = \App\KlasifikasiRakyat::where('lantai',$lantai)->where('kategori',$klas)->count();
    	$plantai = $jenislantai/$jumlahklas;
        // return $plantai;

    	//p($daging|Sedang)
    	//menghitung varian data bangun sedang
        $avgdaging = DB::table('klasifikasirakyat')->where('kategori',$klas)->avg('daging');//rata rata tinggi bangunan sedang
        $sum=0; //sigma
        $data = DB::table('klasifikasirakyat')->select('daging')->where('kategori',$klas)->get();
            foreach($data as $row){
                $sum+=pow($row->daging-$avgdaging,2);
            }
        $varian = $sum/($jumlahklas-1);//varian
        $likelihooddaging= $this->likelihood($varian,$daging,$avgdaging); //likelihood bangunan sedang
        $pdaging = $prior*$likelihooddaging;
        // return $avgdaging;

        $pklas = $prior*$pbangunan*$pbakar*$plantai*$pdaging;
        return $pklas;
    }

}
