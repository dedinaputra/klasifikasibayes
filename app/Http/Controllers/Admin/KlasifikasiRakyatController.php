<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\KlasifikasiRakyat;
use App\Http\Requests\CreateKlasifikasiRakyatRequest;
use App\Http\Requests\UpdateKlasifikasiRakyatRequest;
use Illuminate\Http\Request;



class KlasifikasiRakyatController extends Controller {

	/**
	 * Display a listing of klasifikasirakyat
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $klasifikasirakyat = KlasifikasiRakyat::all();

		return view('admin.klasifikasirakyat.index', compact('klasifikasirakyat'));
	}

	/**
	 * Show the form for creating a new klasifikasirakyat
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    
	    
        $bakar = KlasifikasiRakyat::$bakar;
        $lantai = KlasifikasiRakyat::$lantai;
        $kategori = KlasifikasiRakyat::$kategori;

	    return view('admin.klasifikasirakyat.create', compact("bakar", "lantai", "kategori"));
	}

	/**
	 * Store a newly created klasifikasirakyat in storage.
	 *
     * @param CreateKlasifikasiRakyatRequest|Request $request
	 */
	public function store(CreateKlasifikasiRakyatRequest $request)
	{
	    
		KlasifikasiRakyat::create($request->all());

		return redirect()->route(config('quickadmin.route').'.klasifikasirakyat.index');
	}

	/**
	 * Show the form for editing the specified klasifikasirakyat.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$klasifikasirakyat = KlasifikasiRakyat::find($id);
	    
	    
        $bakar = KlasifikasiRakyat::$bakar;
        $lantai = KlasifikasiRakyat::$lantai;
        $kategori = KlasifikasiRakyat::$kategori;

		return view('admin.klasifikasirakyat.edit', compact('klasifikasirakyat', "bakar", "lantai", "kategori"));
	}

	/**
	 * Update the specified klasifikasirakyat in storage.
     * @param UpdateKlasifikasiRakyatRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateKlasifikasiRakyatRequest $request)
	{
		$klasifikasirakyat = KlasifikasiRakyat::findOrFail($id);

        

		$klasifikasirakyat->update($request->all());

		return redirect()->route(config('quickadmin.route').'.klasifikasirakyat.index');
	}

	/**
	 * Remove the specified klasifikasirakyat from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		KlasifikasiRakyat::destroy($id);

		return redirect()->route(config('quickadmin.route').'.klasifikasirakyat.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            KlasifikasiRakyat::destroy($toDelete);
        } else {
            KlasifikasiRakyat::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.klasifikasirakyat.index');
    }

}
