<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\Rakyat;
use App\Http\Requests\CreateRakyatRequest;
use App\Http\Requests\UpdateRakyatRequest;
use Illuminate\Http\Request;



class RakyatController extends Controller {

	/**
	 * Display a listing of rakyat
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $rakyat = Rakyat::all();

		return view('admin.rakyat.index', compact('rakyat'));
	}

	/**
	 * Show the form for creating a new rakyat
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    
	    
	    return view('admin.rakyat.create');
	}

	/**
	 * Store a newly created rakyat in storage.
	 *
     * @param CreateRakyatRequest|Request $request
	 */
	public function store(CreateRakyatRequest $request)
	{
	    
		Rakyat::create($request->all());

		return redirect()->route(config('quickadmin.route').'.rakyat.index');
	}

	/**
	 * Show the form for editing the specified rakyat.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$rakyat = Rakyat::find($id);
	    
	    
		return view('admin.rakyat.edit', compact('rakyat'));
	}

	/**
	 * Update the specified rakyat in storage.
     * @param UpdateRakyatRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateRakyatRequest $request)
	{
		$rakyat = Rakyat::findOrFail($id);

        

		$rakyat->update($request->all());

		return redirect()->route(config('quickadmin.route').'.rakyat.index');
	}

	/**
	 * Remove the specified rakyat from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		Rakyat::destroy($id);

		return redirect()->route(config('quickadmin.route').'.rakyat.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            Rakyat::destroy($toDelete);
        } else {
            Rakyat::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.rakyat.index');
    }

}
